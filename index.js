console.log("ES6 Updates:");

	// [Section] Exponent Operator
	// Before ES6
		const firstNum = 8**2;
		console.log(firstNum)

	// ES6
		/*
			Syntax: Math.pow(base, exponent);
		*/

		const secondNum = Math.pow(8,2);
		console.log(secondNum);

	// [Section] Template Literals
		// allows us to write strings without using concatenation operator (+);
		// greatly helps with the code readibility

	let name = 'John'
		// Before ES6
		let message = "hello " + name + "! Welcome to programming"
		console.log(message)

		// After ES6
		// uses backticks(``)
		message = `hello ${name}! Welcome to programming.`
		console.log(message)

		// Template literals allows us to perform operations.

		const interestRate = 0.1;
		const principal = 1000;
		console.log(`The interest on you saving account is: ${interestRate*principal}.`);

	// [Section] Array Destructuring
		// allows us to unpack elements in an array into distinct variables
		// allows us to name array elements with variables instead of using index number
		/*
			Syntax:
				let/const [variableNameA, variableNameB, ...] = arrayName;
		*/

	const fullName = ["juan", "dela", "cruz"];

		// before ES6
		let firstName = fullName[0]
		let middleName = fullName[1]
		let lastName = fullName[2]
		console.log(`Hello ${firstName} ${middleName} ${lastName}`)

		// After ES6 updates
		const [fName, mName, lName] = fullName;

		console.log(fName);
		console.log(mName);
		console.log(lName);

		// Mini-activity
			// Array destructuring
		let array = [1, 2, 3, 4, 5];

		let [Num1, Num2, thirdNum, fourthNum, fifthNum] = array;

		console.log(Num1);
		console.log(Num2);
		console.log(thirdNum);
		console.log(fourthNum);
		console.log(fifthNum);

	// [Section] Object Destructuring
		// allows us to unpack properties of objects into distinct variables
		/*
			Syntax:
				let/const {propertyNameA, propertyNameB, . . . } = objectName
		*/

		const person = {
			givenName: "Jane",
			maidenName: "Dela",
			familyName: "Cruz"
		}
			// Before ES6
			let gName = person.givenName;
			let midName = person.maidenName;
			let famName = person.familyName;
			console.log(gName)
			console.log(midName)
			console.log(famName)

			// After ES6 update

			let {givenName, maidenName, familyName} = person;
			console.log("Object destructuring after ES6");
			console.log(givenName);
			console.log(maidenName);
			console.log(familyName);

	// [Section] Arrow Function
		// Compact alternative syntax to traditional functions

	const hello = () =>{
		console.log("Hello World")
	}
	hello()

	/* function expression

	const hello = function(){
		console.log("hello world")
	}

	function declaration

	function hello (){
		console.log("hello world")
	}*/

	// [Section] Implicit Return
	/*
		there are instances when you can commit return statement
		this works because even without using return keyword.
	*/

	const add = (x, y) => x+y; //single line function
	console.log("Implicit return")
	console.log(add(1,2))

	const substract = (x,y) => { //single line function with return
		return x-y
	}
	console.log(substract(10,5))

	// [Section] Default Function Argument Value

		/*const greet = (firstName = "firstName", lastName = "lastName") => {
			return `Good Morning, ${firstName} ${lastName}!`;
		}
		console.log(greet("William", "Mirafuentes"));*/

		function greet(firstName, lastName){
			return `Good Morning, ${firstName} ${lastName}!`;
		}
		console.log(greet())

	// [Section] Class-based Object Blueprints
		// allows us to create/instantiation of objects using classes blueprints
			// constructor is a special method of a class for creating/initializing an object of the class
		/*
			Syntax:
				class className{
					constructor(objectValueA, objectValueB, . . .){
						this.objectPropertyA = objectValueA;
						this.objectPropertyB = objectValueB;
					}
				}
		*/

		class Car{
			constructor(brand, name, year){
				this.carBrand = brand;
				this.carName = name;
				this.carYear = year;
			}
		}

		let car = new Car("Toyota", "Hilux-pickup", 2015)
		console.log(car)

		car.carBrand = "Nissan"
		console.log(car)